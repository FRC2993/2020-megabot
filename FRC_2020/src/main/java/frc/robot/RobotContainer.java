/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.ExampleSubsystem;
import frc.robot.subsystems.Lift;
import frc.robot.subsystems.Pickup;
import frc.robot.subsystems.Shooter;
import frc.robot.commands.driveTrain.rotateToVisionTarget;
import frc.robot.vision.Vision;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
 Shooter shooter = new Shooter();
 DriveTrain driveTrain = new DriveTrain(); 
 Pickup pickup = new Pickup();
 Vision vision = new Vision();
 
 CommandBase visionRotateTest = new rotateToVisionTarget(vision, driveTrain);
 Lift lift = new Lift();
 Spark spark = new Spark(1);
 
 PressureMoniter pressureMoniter = new PressureMoniter(); 
  // The robot's subsystems and commands are defined here...
  private final ExampleSubsystem m_exampleSubsystem = new ExampleSubsystem();

  //private final ExampleCommand m_autoCommand = new InstantCommand(pickup.enableBackAndForth(on))

  // passengerJoystick = new Joystick(1);
  XboxController driveController = new XboxController(0);
  XboxController passngeController = new XboxController(1);


  public enum driveMode{
      arcade, tank
  };

  driveMode currentMode = driveMode.arcade; 

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
   
    if(DriverStation.getInstance().getAlliance() == Alliance.Blue){
      spark.set(0.87);
    }
    else{
      spark.set(0.61);
    }
    //set default command to joystick drive
    driveTrain.setDefaultCommand(new RunCommand(() -> 
      driveTrain.arcadeDrive(driveController.getY(Hand.kLeft) , driveController.getX(Hand.kRight) ), driveTrain));

    shooter.setDefaultCommand(new RunCommand(() -> 
      shooter.rotateTurret(passngeController.getX(Hand.kLeft) ), shooter));
  

    // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    //run intake
    new JoystickButton(driveController, 1).whileHeld(new SequentialCommandGroup(new Command[]{
      new InstantCommand(() -> pickup.startTimer()),
      new RunCommand(() -> pickup.pickup(), pickup),
  })).whenReleased(new InstantCommand(() -> pickup.stop()));

    //intake piston
    new JoystickButton(driveController, 2).whenPressed(new InstantCommand(() -> pickup.togglePickup()));

    //intake out
    new JoystickButton(driveController, 3).whileHeld(new ParallelCommandGroup(new Command[]{
      new RunCommand(() -> shooter.runBackwords(), shooter),
      new RunCommand(() -> pickup.unload(), pickup)
    })).whenReleased(new ParallelCommandGroup(new Command[]{
      new InstantCommand(() -> pickup.stop()),
      new InstantCommand(() -> shooter.stop())
    }));

    //raise lift
    new JoystickButton(driveController, 4).whileHeld(new RunCommand(() -> lift.raiseLift(passngeController), lift))
    .whenReleased(new InstantCommand(() -> lift.resetUsed()));

    //toggle slow
    new JoystickButton(driveController, 5).whenPressed(new InstantCommand(() -> driveTrain.toggleSlow()));

    //toggle invert
    new JoystickButton(driveController, 6).whenPressed(new InstantCommand(() -> driveTrain.toggleInvet(spark)));
    

    //run intake
    new JoystickButton(passngeController, 1).whileHeld(new SequentialCommandGroup(new Command[]{
      new InstantCommand(() -> pickup.startTimer()),
      new RunCommand(() -> pickup.pickup(), pickup)
  })).whenReleased(new InstantCommand(() -> pickup.stop()));

    //intake piston
    new JoystickButton(passngeController, 2).whenPressed(new InstantCommand(() -> pickup.togglePickup()));

    //intake out
    new JoystickButton(passngeController, 3).whileHeld(new ParallelCommandGroup(new Command[]{
      new RunCommand(() -> shooter.runBackwords(), shooter),
      new RunCommand(() -> pickup.unload(), pickup)
    })).whenReleased(new ParallelCommandGroup(new Command[]{
      new InstantCommand(() -> pickup.stop()),
      new InstantCommand(() -> shooter.stop())
    }));

    //close shoot
    new JoystickButton(passngeController, 5).whileHeld(new SequentialCommandGroup(new Command[]{
      new InstantCommand(() -> shooter.setSpeed(1900)), //TODO change to 2500
      new InstantCommand(() -> pickup.enableBackAndForth(true)),
      new InstantCommand(() -> shooter.setFlyEnabled(true)),
      new InstantCommand(() -> shooter.hood(true)),
      new RunCommand(() -> pickup.load(shooter.onTarget()), pickup)
    }))
      .whenReleased(new ParallelCommandGroup( new Command[]{
        new InstantCommand(() -> shooter.hood(false)),
        new InstantCommand(() -> shooter.disable()),
        new InstantCommand(() -> pickup.enableBackAndForth(false)),
        new InstantCommand(() -> pickup.stop())
      }));

    //far shoot
    new JoystickButton(passngeController, 6).whileHeld(new SequentialCommandGroup(new Command[]{
      new InstantCommand(() -> shooter.setSpeed(2900)), //TODO double check this to 2800
      new InstantCommand(() -> pickup.enableBackAndForth(true)),
      new InstantCommand(() -> shooter.setFlyEnabled(true)),
      new InstantCommand(() -> shooter.hood(true)),
      new RunCommand(() -> pickup.load(shooter.onTarget()), pickup)
    }))
      .whenReleased(new ParallelCommandGroup( new Command[]{
        new InstantCommand(() -> shooter.hood(false)),
        new InstantCommand(() -> shooter.disable()),
        new InstantCommand(() -> pickup.enableBackAndForth(false)),
        new InstantCommand(() -> pickup.stop())
      }));
    // new JoystickButton(passengerJoystick, 1).whileHeld(new InstantCommand(() -> shooter.hood(true))).
    // whenReleased(new InstantCommand(() -> shooter.hood(false)));
   

  

    //new JoystickButton(passengerJoystick, 4).whenPressed(new InstantCommand(() -> pickup.togglePickup(false)));
    // new JoystickButton(passengerJoystick, 2).whenPressed(new InstantCommand(() -> shooter.setFlyEnabled(true)));
    // new JoystickButton(passengerJoystick, 4).whenPressed(new InstantCommand(() -> shooter.setFlyEnabled(false)));
    // new JoystickButton(driveController, 5).whenPressed(new InstantCommand(() -> lift.raiseLift()));
    // new JoystickButton(driveController, 6).whenPressed(new InstantCommand(() -> lift.lowerLift()));
    
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return new InstantCommand(() -> shooter.setSpeed(1900))
    .andThen(new InstantCommand(() -> shooter.enable()))
    .andThen(new InstantCommand(() -> shooter.hood(true)))
    .andThen(new RunCommand(() -> pickup.load(shooter.onTarget()), pickup))
    .withTimeout(5)
    .andThen(new InstantCommand(() -> shooter.disable()))
    .andThen(new InstantCommand(() -> shooter.hood(false)))
    .andThen(new RunCommand(() -> driveTrain.arcadeDrive(-0.6, 0), driveTrain)) //negative goes toward goal
    .withTimeout(7.5); //time for withTimeout includes the time other commands spent running, meaning that this will actually back up for 2.5 seconds
  }
}
  
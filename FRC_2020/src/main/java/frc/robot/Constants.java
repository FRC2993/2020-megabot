/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

public final class Constants {
    public static final double LOW_GEAR_RATIO = (58/22.0);
    public static final double MED_GEAR_RATIO = (60/20.0);
    public static final double HIGH_GEAR_RATIO = (62/18.0);

    public static double unitsToShaftRpm(double units){
        return units * (600.0 / 2048);
    }

    public static double refactorDegrees(double degrees){
        
        degrees =  degrees % 360;
        // if (degrees < 0){
        //     degrees = 360 + degrees; 
        // }
        return degrees; 
    }

    

    

}

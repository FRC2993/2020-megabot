/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.vision;

import java.util.ArrayList;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import edu.wpi.cscore.AxisCamera;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoMode.PixelFormat;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.vision.VisionPipeline;
import edu.wpi.first.vision.VisionThread;
import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Relay.Direction;
import edu.wpi.first.wpilibj.Relay.Value;


/**
 * Add your docs here.
 */
public class Vision implements VisionPipeline {

    public final int EXPOSURE = 0;
    public final int BRIGHTNESS = 0;
    public static final int CAMERA_HEIGHT = 240;
    public static final int CAMERA_WIDTH = 320;
    public static final double X_FIELD_OF_VIEW = 100.0; //TODO change this

    private boolean firstFrameProcessed = false; 
    //private UsbCamera camera;
    private AxisCamera axisCamera;
    //private VisionThread visionThread;
    private TargetGripPipeline targetGripPipeline = new TargetGripPipeline();

    int visionTargetXOffset = 0;
    int visionTargetYOffset = 0;
    
    private CvSource cvSource = null;
    private final Object padlock = new Object();

    private final Relay lightRelay = new Relay(0, Direction.kForward);

    public Vision(){
        //camera = CameraServer.getInstance().startAutomaticCapture(); 
        //axisCamera = new AxisCamera("camera", "axis-camera");
        //axisCamera = CameraServer.getInstance().addAxisCamera("axis-camera");
        axisCamera = CameraServer.getInstance().addAxisCamera("10.29.93.11");
        axisCamera.setExposureAuto();
        axisCamera.setBrightness(100);
        axisCamera.setResolution(CAMERA_WIDTH, CAMERA_HEIGHT);
        //axisCamera.setPixelFormat(PixelFormat.kGray);
        //camera.setBrightness(BRIGHTNESS);
        //camera.setExposureManual(EXPOSURE);
        //camera.setResolution(CAMERA_WIDTH, CAMERA_HEIGHT);

        //visionThread = new VisionThread(axisCamera, this, pipeline -> AdditionalVisionProcessing(pipeline));
        //visionThread.start();
        turnOnLight();
    }

    public void turnOnLight(){
        lightRelay.set(Value.kOn);
    }

    public void turnOffLight(){
        lightRelay.set(Value.kOff);
    }

    public void reset(){
      firstFrameProcessed = false; 
    }

    public void AdditionalVisionProcessing(Vision pipeline) {
            // Create a video stream, visible to the dashboard, that will hold our vision pipeline output
    if (cvSource == null) {
        cvSource = CameraServer.getInstance().putVideo("Vision",
          targetGripPipeline.hsvThresholdOutput().width(),
          targetGripPipeline.hsvThresholdOutput().height());
      }
  
      // Convert coutours to bounding rects
      ArrayList<Rect> rects = new ArrayList<Rect>();
      for (int i = 0; i < targetGripPipeline.filterContoursOutput().size(); i++) {
        MatOfPoint contour = targetGripPipeline.filterContoursOutput().get(i);
        MatOfPoint2f tmp = new MatOfPoint2f();
        contour.convertTo(tmp, CvType.CV_32F);
        rects.add(Imgproc.boundingRect(tmp));
  
        // Also draw each contour to the hsvThreshold Mat
        Imgproc.drawContours(targetGripPipeline.hsvThresholdOutput(), targetGripPipeline.filterContoursOutput(), i++, new Scalar(255, 255, 255));
      }
  
      // Sort rects by area largest to smallest
      rects.sort((r1, r2) -> Double.compare(r2.area(), r1.area()));
  
      // Determine x and y vlaue of the image center
      int frameCenterX = targetGripPipeline.hsvThresholdOutput().width() / 2;
      int frameCenterY = targetGripPipeline.hsvThresholdOutput().height() / 2;
  
      // Draw cross on image at center of frame
      Imgproc.drawMarker(targetGripPipeline.hsvThresholdOutput(),
        new Point(frameCenterX, frameCenterY),
        new Scalar(255, 0, 0),
        0);
  
      // If we have at least 1 rectangle, try to process it in relation to the screen center
      if (rects.size() > 0) {
        // The the first (largest) rectangle
        Rect targetRect = rects.get(0);
  
  
        // Calculate where we want to aim, as the center of the top of the rectangle
        int targetX = targetRect.x + targetRect.width / 2;
        int targetY = targetRect.y;
  
        synchronized(padlock) {
          // Calculate X offset so that a target LEFT of frame center will produce a negative offset value
          // so that we rotate counter-clockwise to try to reduce the offset
          visionTargetXOffset = targetX - frameCenterX;
  
          // Calculate Y offset so that a target ABOVE frame center will produce a negative offset value
          // so that we drive backwards to try to reduce the offset
          visionTargetYOffset = targetY - frameCenterY;
        }

        
  
        // Draw start on image at target location
        Imgproc.drawMarker(targetGripPipeline.hsvThresholdOutput(),
          new Point(targetX, targetY),
          new Scalar(0, 255, 255),
          2);
  
        // Draw vector from camera center to target representing the direction the frame must
        // move to align with target
        Imgproc.arrowedLine(targetGripPipeline.hsvThresholdOutput(),
          new Point(frameCenterX, frameCenterY),
          new Point(frameCenterX + visionTargetXOffset, frameCenterY + visionTargetYOffset),
          new Scalar(255, 255, 255));
  
      } else {
        // Hmmm, we don't see a target.  We'll set the offset as if it were just off-screen to the left
        // which would cause us to rotate counter-clockwise to try to find it
        synchronized(padlock) {
          visionTargetXOffset = -frameCenterX;
          visionTargetYOffset = 0;
        }
      }
  
      // Put our annotated image into the video source so the dashboard can view it
      cvSource.putFrame(targetGripPipeline.hsvThresholdOutput());
      firstFrameProcessed = true;   
      

    }

    public boolean getFirstFrameProcessed(){
      return firstFrameProcessed; 
    }

    @Override
    public void process(Mat image) {
        targetGripPipeline.process(image);

    }
    
    public double pixleToDegree(double pixles){
        return (CAMERA_WIDTH / X_FIELD_OF_VIEW) * pixles;
    }

    public int getXOffset(){
        return visionTargetXOffset;
    }

    public double getAngleOffsetX(){
      return -1 * pixleToDegree((double)getXOffset()) ;
    }

    

    public int getYOffset(){
        return visionTargetYOffset; 
    }

}

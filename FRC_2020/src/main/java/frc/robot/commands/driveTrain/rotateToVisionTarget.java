/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.driveTrain;

import edu.wpi.first.wpilibj.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj2.command.ProfiledPIDCommand;
import frc.robot.subsystems.DriveTrain;
import frc.robot.vision.Vision;


public class rotateToVisionTarget extends ProfiledPIDCommand {
  public static final double P = 0.0005;
  public static final double I = 0;
  public static final double D = 0;
  public static final double FIELD_OF_VIEW = 100; //total field of view on x axis in degrees
  public final static double MaxVelocity = 0;
  public final static double MaxAccelerations = 0; 

  public Vision vision; 
  /**
   * Creates a new rotateToVisionTarget.
   */
  public rotateToVisionTarget(Vision vision, DriveTrain driveTrain) {
    
    super(new ProfiledPIDController(P, I, D, new TrapezoidProfile.Constraints(MaxVelocity, MaxAccelerations)), 
    () -> vision.getXOffset(), 0 , (output, setpoint) -> driveTrain.arcadeDrive(0, -output), driveTrain); 

    

    // Use addRequirements() here to declare subsystem dependencies.
    getController().setTolerance(Vision.CAMERA_WIDTH / 20);
    this.vision = vision;
    
  }

  public static double pixelsToDegrees(double pixels){
    return pixels * (Vision.CAMERA_WIDTH / FIELD_OF_VIEW); 
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    vision.turnOnLight();
    vision.reset();
    super.initialize();
  
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(vision.getFirstFrameProcessed()){
      super.execute();
    }
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    vision.turnOffLight();

    super.end(interrupted);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return getController().atGoal();
   
  }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.ColorMatch;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ColorWheel extends SubsystemBase {
    WPI_TalonSRX colorwheelMotor;
    Solenoid releasemotor;
    ColorSensorV3 colorsensor1;

private final I2C.Port i2cPort = I2C.Port.kOnboard;
final ColorSensorV3 colorsensor2 = new ColorSensorV3(i2cPort);
  
    final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
    final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
    final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
    final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);

boolean pushmotor = false;
int colorwheel_Motor = 1;


public ColorWheel() {

  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

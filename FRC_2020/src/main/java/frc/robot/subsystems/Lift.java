/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.XboxController;

public class Lift extends SubsystemBase {
//  Solenoid leftSolenoid = new Solenoid(2);
  Solenoid rightSolenoid = new Solenoid(2);
  private boolean liftEnabled = false;
  private boolean used = false; 
  public Lift() {
  
  }
  public void raiseLift(XboxController controller){
    if(controller.getYButton() && liftEnabled == false && used == false){
      liftEnabled = true;
     // leftSolenoid.set(true);
      rightSolenoid.set(true);
      used = true;
    }
    
    else if(liftEnabled && used == false){
        liftEnabled = false;
        //leftSolenoid.set(false);
      rightSolenoid.set(false);
      used = true;
    }


    
  }

  public void resetUsed(){
    used = false;
  }

  

  public void lowerLift(){
    //leftSolenoid.set(false);
    rightSolenoid.set(false);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

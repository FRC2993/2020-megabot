/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.Controller;
import edu.wpi.first.wpilibj.PIDBase;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.PIDBase.Tolerance;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj2.command.PIDSubsystem;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.Constants;

public class Shooter extends PIDSubsystem {
  WPI_TalonFX flywheelMotorA; 
  WPI_TalonFX flywheelMotorB;
  WPI_TalonSRX aimMotor = new WPI_TalonSRX(8);
  Solenoid hoodSolenoid;
  double playerControlledSpeed = 0;
  int speed = 2500;
  //TODO change these to correct value
  final int SHOOTER_A_ID = 9; 
  final int SHOOTER_B_ID = 10; 
  //final int SHOOTER_C_ID = 0; TODO uncommet this
  final static double P = 0.001; //original 1.65
  final static double I = 0;
  final static double D = 0.0;

  final static double shooterKs = 0.345;
  final static double shooterKv = 0.113;

  private final SimpleMotorFeedforward simpleMotorFeedforward = new SimpleMotorFeedforward(shooterKs, shooterKv);
  boolean onSpeed = false; 
  double tolerance = (speed * 0.1) / 60;
  double shooterRpm = 1;
  double aimControl = 1;
  boolean flyEnabled = false;

  public Shooter() {
    super(new PIDController(P, I, D));
    getController().setTolerance(tolerance, tolerance);
    setSetpoint(speed / 60);
    
    
    
    // super(new ProfiledPIDController(P, I, D, new TrapezoidProfile.Constraints(0, 0)));
      flywheelMotorA = new WPI_TalonFX(SHOOTER_A_ID);
      flywheelMotorB = new WPI_TalonFX(SHOOTER_B_ID);
      flywheelMotorB.setInverted(true);
      flywheelMotorA.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
      flywheelMotorB.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
      flywheelMotorA.setNeutralMode(NeutralMode.Coast);
      flywheelMotorB.setNeutralMode(NeutralMode.Coast);
      
      hoodSolenoid = new Solenoid(1);
      

    

      
      
  }
  public double getVelocityAverage(){
    double shaftRpm = (Constants.unitsToShaftRpm(flywheelMotorA.getSelectedSensorVelocity()) ) / 60.0;
    SmartDashboard.putNumber("shaft", shaftRpm * 60);
   return shaftRpm;

    
  }
  public void shoot(final double speed) {
    flywheelMotorA.set(speed);
    flywheelMotorB.set(speed);
  }

  public void hood(final boolean hoodUp) {
    hoodSolenoid.set(hoodUp);
  }

  public void rotateAim(final double speed){
       aimMotor.set(speed);
  }

  public void setPlayerControlledSpeed(double playerControlledSpeed) {
    setSetpoint(playerControlledSpeed / 60);
  }

  public double getPlayerControlledSpeed() {
    return playerControlledSpeed;
  }

  public void setSpeed(double speed){
    speed = speed / 60;
    setSetpoint(speed);
    getController().setTolerance(speed * 0.1);
  }
  
  public void setFlyEnabled(Boolean flyEnabled) {
    if(flyEnabled){
      enable();
    }
    else{
      disable();
    }
  }

  public void runBackwords(){
    flywheelMotorA.set(-0.2);
    flywheelMotorB.set(-0.2);
  }

  public double calculateMinVolatage(double setpoint){
      return (12/13000.0) * setpoint;
  }

  public void stop(){
    flywheelMotorA.set(0);
    flywheelMotorB.set(0);
  }
  
  public boolean onTarget(){
    boolean onTarget = getController().atSetpoint();
    
    return onTarget;

    // if(getVelocityAverage() > (speed / 60) - tolerance && getVelocityAverage() < (speed / 60) + tolerance){
    //   return true;
    // }
    // return false;
    
    
  }
  @Override
  public void periodic() {
    super.periodic();
    
    // This method will be called once per scheduler run
  }

  @Override
  protected void useOutput(double output, double setpoint) {
      
      double feedForwardTerm = simpleMotorFeedforward.calculate(setpoint);
     // getVelocityAverage();

     // output = MathUtil.clamp(output + feedForwardTerm, calculateMinVolatage(setpoint), 12); //keeps output volatage at high enough value to maintain wheel speed
    
      //feed forward was giving too high of a value
      // flywheelMotorA.setVoltage(output + feedForwardTerm);
      // flywheelMotorB.setVoltage(output + feedForwardTerm);
        flywheelMotorA.setVoltage(output + feedForwardTerm);
        flywheelMotorB.setVoltage(output + feedForwardTerm);
      onSpeed = this.onTarget();
      
    


  }

  public void rotateTurret(double speed){
    aimMotor.set(speed * 0.3);
  }

  @Override
  protected double getMeasurement() {
  
    return getVelocityAverage();
  }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


public class Pickup extends SubsystemBase {


  int upTime = 250;
  int downTimer = 0;


  Solenoid loweringSolenoid;
  WPI_TalonSRX feedMotor = new WPI_TalonSRX(5);
  WPI_TalonSRX loadingMotor = new WPI_TalonSRX(6);
  WPI_TalonSRX loadingMotorTop = new WPI_TalonSRX(7);
  public Solenoid pickupSolenoid = new Solenoid(4);
  private boolean pickupOn = false;
  double feedSpeed = 0.5;//TODO change these eventually
  double loadingSpeed = 0.5;
  long startTime;
  
  public Pickup() {
    

  }

  public void togglePickup(){
      pickupOn = !pickupOn; 
      pickupSolenoid.set(pickupOn);
  }

  public void pickup(){
    feedMotor.set(feedSpeed);
    load(true);
  }

  public void load(boolean load){
    if(load == false){
      stop();
    }
    long runTime = System.currentTimeMillis() - startTime;
    
    if(load && runTime < upTime){
      loadingMotor.set(loadingSpeed * -1);
      loadingMotorTop.set(loadingSpeed + 0.2);
    }
    else if(load && runTime < upTime + downTimer){
      loadingMotor.set(loadingSpeed );
      loadingMotorTop.set((loadingSpeed + 0.2) * -1);
      // feedMotor .set(0);
    }
    if(runTime > upTime + downTimer){
      startTimer();
    }
    
    
  }

  public void unload(){
    loadingMotorTop.set(-0.5);
    loadingMotor.set(0.5);
  }

  public void stop(){
    feedMotor.set(0);
    // lowerPickup(false);
    //load(false);
    loadingMotor.set(0);
    loadingMotorTop.set(0);
  }

  public void startTimer(){
      startTime = System.currentTimeMillis();
  }

  public void enableBackAndForth(boolean on){
    if(on){
      downTimer = 50;
  
    }
    else{
      downTimer =  0;
    }
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}

/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;


public class DriveTrain extends SubsystemBase{
  WPI_TalonFX left1; 
  WPI_TalonFX left2; 
  WPI_TalonFX right1; 
  WPI_TalonFX right2; 

  //TODO set these to correct values
  final int LEFT1_ID = 3;
  final int LEFT2_ID = 2;
  final int RIGHT1_ID = 1; 
  final int RIGHT2_ID = 4; 

  private final double MAX_SPEED = 0.65; 
  private final double SLOW_SPEED = 0.4; 

  private boolean slow = false; 
  private boolean inverted = false; 

  private int invertNumber = 1;

  SpeedControllerGroup leftGroup; 
  SpeedControllerGroup rightGroup; 

  DifferentialDrive drive; 

  ADXRS450_Gyro gyro = new ADXRS450_Gyro(); 



  public DriveTrain() {

      left1 = new WPI_TalonFX(LEFT1_ID);
      left2 = new WPI_TalonFX(LEFT2_ID);
      right1 = new WPI_TalonFX(RIGHT1_ID);
      right2 = new WPI_TalonFX(RIGHT2_ID); 

      left1.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
      left2.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
      right1.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);
      right2.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor);

      left1.configOpenloopRamp(1.5);
      left2.configOpenloopRamp(1.5);
      right1.configOpenloopRamp(1.5);
      right2.configOpenloopRamp(1.5);
      
      leftGroup = new SpeedControllerGroup(left1, left2);
      rightGroup = new SpeedControllerGroup(right1, right2); 

      drive = new DifferentialDrive(leftGroup, rightGroup);
      drive.setDeadband(0.2);
      drive.setMaxOutput(0.60);

      
      
  }
  

  public void arcadeDrive(double forward, double rotation){
      drive.arcadeDrive(forward * invertNumber, rotation * 0.75 , true);

  }

  public int getAverageMotorPosition(){
      int left1Position = left1.getSelectedSensorPosition();
      int left2Position = left2.getSelectedSensorPosition();
      int right1Position = right1.getSelectedSensorPosition();
      int right2Position = right2.getSelectedSensorPosition();
      return (left1Position + left2Position + right1Position + right2Position)/4;
  }

  public void resetMotorEncoders(){
    left1.setSelectedSensorPosition(0);
    left2.setSelectedSensorPosition(0);
    right1.setSelectedSensorPosition(0);
    right2.setSelectedSensorPosition(0);
  }

  public double getGyroAngle(){
    return Constants.refactorDegrees(gyro.getAngle());
  }

  public void toggleSlow(){
    if(slow){
        slow = false;
        drive.setMaxOutput(SLOW_SPEED);
    }
    else{
      slow = true;
      drive.setMaxOutput(MAX_SPEED);
    }
    SmartDashboard.putBoolean("slow", slow);
  }

  public void toggleInvet(Spark spark){
      if(inverted){
        inverted = false; 
        invertNumber = -1;
        if(DriverStation.getInstance().getAlliance() == Alliance.Blue){
          spark.set(0.87);
        }
        else{
          spark.set(0.61);
        }
       
      }
      else{
        if(DriverStation.getInstance().getAlliance() == Alliance.Blue){
          spark.set(-0.09);
        }
        else{
          spark.set(-0.11);
        }
        inverted = true;
        invertNumber = 1;
        //spark.set(0.67);
      }
      SmartDashboard.putBoolean("inverted", inverted);
  }

  public void resetGyro(){
    gyro.reset(); 
  }
}